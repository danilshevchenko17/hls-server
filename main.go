package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
)

func start(w http.ResponseWriter, req *http.Request) {
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	type url struct {
		URL string
	}

	var Url url
	json.Unmarshal([]byte(reqBody), &Url)

	downloadFile("upload/video.mp4", Url.URL)

	ffmpegExec()

	openBrowser("localhost:8080")
}

func clean(w http.ResponseWriter, req *http.Request) {
	e := os.RemoveAll("upload/")
	if e != nil {
		log.Fatal(e)
	}
	e = os.RemoveAll("static/")
	if e != nil {
		log.Fatal(e)
	}
	os.MkdirAll("upload/", 0755)
	os.MkdirAll("static/", 0755)
}

func ffmpegExec() {
	cmdArguments := []string{"-i", "upload/video.mp4", "-c:v", "h264", "-flags", "+cgop", "-g", "30", "-hls_time", "1", "static/out.m3u8"}
	cmd := exec.Command("ffmpeg", cmdArguments...)
	cmd.Run()
}

func downloadFile(filepath string, url string) {

	out, err := os.Create(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("bad status: %s", resp.Status)
		if err != nil {
			log.Fatal(err)
		}
	}

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		log.Fatal(err)
	}
}

func openBrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}

}

func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/upload", start)
	http.HandleFunc("/clean", clean)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
