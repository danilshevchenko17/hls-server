# HLS-Server

## Build

git clone https://gitlab.com/danilshevchenko17/hls-server.git

go build main.go

## Usage

./main

curl -i -X POST -H 'Content-Type: application/json' -d '{"URL" : "https://download.samplelib.com/mp4/sample-5s.mp4" } 'localhost:8080/upload

curl -i -X POST localhost:8080/clean